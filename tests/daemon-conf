#!/bin/sh
# Get coverage of libvirtd's config-parsing code.

test -z "$srcdir" && srcdir=$(pwd)
test -z "$abs_top_srcdir" && abs_top_srcdir=$(pwd)/..
test -z "$abs_top_builddir" && abs_top_builddir=$(pwd)/..

if test "$VERBOSE" = yes; then
  set -x
  $abs_top_builddir/daemon/libvirtd --version
fi

. "$srcdir/test-lib.sh"

test -z "$CONFIG_HEADER" && CONFIG_HEADER="$abs_top_builddir/config.h"

grep '^#define WITH_QEMU 1' "$CONFIG_HEADER" > /dev/null ||
  skip_test_ "configured without QEMU support"

conf="$abs_top_srcdir/daemon/libvirtd.conf"

# Ensure that each commented out PARAMETER = VALUE line has the expected form.
grep '[a-z_]  *=  *[^ ]' "$conf" | grep -vE '^#[a-z_]+ = ' \
  && { echo "$0: found unexpected lines (above) in $conf" 1>&2; exit 1; }

# Start with the sample libvirtd.conf file, uncommenting all real directives.
sed -n 's/^#\([^ #]\)/\1/p' "$conf" > tmp.conf

# Iterate through that list of directives, corrupting one RHS at a
# time and running libvirtd with the resulting config.  Each libvirtd
# invocation must fail.
n=$(wc -l < tmp.conf)
i=1
while :; do
  param_name=$(sed -n "$i"'s/ = .*//p' tmp.conf)
  printf "testing with corrupted config: $param_name\n" 1>&2
  rhs=$(sed -n "$i"'s/.* = \(.*\)/\1/p' tmp.conf)
  f=in$i.conf
  case $rhs in
    # Change an RHS that starts with '"' or '[' to "3".
    [[\"]*) sed "$i"'s/ = [["].*/ = 3/' tmp.conf > $f;;
    # Change an RHS that starts with a digit to the string '"foo"'.
    [0-9]*) sed "$i"'s/ = [0-9].*/ = "foo"/' tmp.conf > $f;;
  esac

  # Run libvirtd, expecting it to fail.
  $abs_top_builddir/daemon/libvirtd --config=$f 2> err && fail=1

  case $rhs in
    # '"'*) msg='should be a string';;
    '"'*) msg='invalid type: got long; expected string';;
    [0-9]*) msg='invalid type: got string; expected long';;
    '['*) msg='must be a string or list of strings';;
    *) echo "unexpected RHS: $rhs" 1>&2; fail=1;;
  esac

  test $i = $n && break

  # Check that the diagnostic we want appears
  grep "$msg" err 1>/dev/null 2>&1
  RET=$?
  test "$RET" = "0" || fail=1

  i=$(expr $i + 1)
done

# Run with the unmodified config file.
sleep_secs=2

# Be careful to specify a non-default socket directory:
sed 's,^unix_sock_dir.*,unix_sock_dir="'"$(pwd)"'",' tmp.conf > k || fail=1
mv k tmp.conf || fail=1

# Also, specify a test-specific log directory:
sed 's,^log_outputs.*,log_outputs="3:file:'"$(pwd)/log"'",' tmp.conf > k \
    || fail=1
mv k tmp.conf || fail=1

printf "running libvirtd with a valid config file ($sleep_secs seconds)\n" 1>&2
$abs_top_builddir/daemon/libvirtd --pid-file=pid-file --config=tmp.conf > log 2>&1 & pid=$!
sleep $sleep_secs
kill $pid

# Expect an orderly shut-down and successful exit.
wait $pid || fail=1

# "cat log" would print this for non-root:
#   Cannot set group when not running as root
#   Shutting down on signal 15
# And normally, we'd require that output, but obviously
# it'd be different for root.

exit $fail
